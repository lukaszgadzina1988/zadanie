<?php


namespace App\Service;


use App\Traits\ApiRequestTrait;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class OpenWheatherService
{
    use ApiRequestTrait;

    private $apiUrl;

    private $apiKey;

    public function __construct(ParameterBagInterface $params)
    {
        $this->apiUrl = $params->get('openwheather_api_key');
        $this->apiKey = $params->get('openwheather_api_url');
    }

    /**
     * Method getting data from openwheatherapi for city
     * @param string $city
     * @return array|bool|string
     */
    public function getCityTemperature(string $city)
    {
        $this->setUrl($this->apiUrl);
        $this->setApiKey($this->apiKey);

        return $this->requestCityToApi($city);
    }

}
