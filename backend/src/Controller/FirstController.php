<?php


namespace App\Controller;


use App\Service\OpenWheatherService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class FirstController
 * @package App\Controller
 *
 * @Route("/api")
 */
class FirstController extends AbstractController
{
    private OpenWheatherService $openWheatherService;

    public function __construct(OpenWheatherService $openWheatherService)
    {
        $this->openWheatherService = $openWheatherService;
    }

    /**
     * @Route("/check-weather", methods={"POST"})
     * @param Request $request - object with requested data
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        $content = $request->getContent();
        $data = json_decode($content);

        $response = $this->openWheatherService->requestCityToApi($data->params->city);

        return new JsonResponse(['data' => $response['data']], $response['cod']);
    }

}
