<?php


namespace App\Traits;

use \ErrorException;

trait ApiRequestTrait
{

    private string $url;

    private string $apiKey;

    public function setUrl(string $url)
    {
        $this->url = $url;
    }

    public function setApiKey(string $apiKey)
    {
        $this->apiKey = $apiKey;
    }

    /**
     * Method request city to API and return values
     *
     * @param string $cityName - name of city which we requested to api
     *
     * @return string | bool - curl response data
     */
    public function requestCityToApi(string $cityName)
    {
        try {
            $c = curl_init();
            $finalUrl = $this->buildFinalUrl($cityName);
            curl_setopt($c, CURLOPT_URL, $finalUrl);
            curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);

            $data = json_decode(curl_exec($c));

            curl_close($c);

            if ($data->cod === 404) {
                return json_encode([
                    'data' => $data->message,
                    'cod' => '404'
                ]);
            }

            return [
                'data' => $data,
                'cod' => $data->cod
            ];
        } catch (ErrorException $e) {
            return json_encode([
                'data' => $e->getMessage(),
                'cod' => '404'
            ]);
        }

    }

    /**
     * Method build param for curl
     *
     * @param string $cityName
     * @return string
     */
    private function buildFinalUrl(string $cityName)
    {
        $params = array('q' => $cityName, 'appid' => $this->apiKey, 'units' => 'metric');

        return $this->url . "?" . http_build_query($params);
    }
}
