import React, {useEffect, useState} from 'react';
import Alert from 'react-bootstrap/Alert';

const Temperature: React.FC<{
  temperature: number,
  city: string
}> = ({
  temperature,
  city
}) => {

  const [variant, setVariant] = useState<string>();

  useEffect(() => {
    const temp = temperature;

    if (temp < 0) {
      setVariant('danger');
    }
    if((temp > 0) && (temp < 10)) {
      setVariant('warning');
    }
    if ((temp > 10) && (temp < 20)) {
      setVariant('primary');
    }
    if (temp > 20) {
      setVariant('success');
    }

  }, [setVariant]);

  return (
    <Alert key={temperature} variant={variant}>
      Temperature in {city} is {temperature}
    </Alert>
  )
}

export default Temperature;
