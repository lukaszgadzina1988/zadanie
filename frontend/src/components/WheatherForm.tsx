import React, {FormEvent, useState} from 'react';
import {request} from "../service/api";
import Toast from 'react-bootstrap/Toast';
import Temperature from "./Temperature";

const WheatherForm = () => {
  const [city, setCity] = useState<string>();
  const [showErrorToast, setShowErrorToast] = useState<boolean>(false);
  const [errorMsg, setErrorMsg] = useState<string>();
  const [temperature, setTemperature] = useState<number>();

  const handleFormSubmit = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    setTemperature(0);
    if (city) {
      request({ city }).then(response => {

        const code = response.data.cod;
        const message = response.data.message;
        const temperature = response.data.data.main.temp;

        if (code === '404') {
          setShowErrorToast(true);
          setErrorMsg(message);
        } else {
          setTemperature(temperature);
        }
      }).catch(error => {
        setShowErrorToast(true);
        setErrorMsg(error['message']);
      });
    }
  };

  return (
    <>
      <div className="row">
        <div className="col-12">
          <label htmlFor="city" className="control-label col-4">
            Type your city
          </label>
          <div className="col-8">
            <form onSubmit={handleFormSubmit}>
              <input id="city" type="text" onChange={event => setCity(event.target.value)} className="form-control" />
              <button type="submit" className="btn btn-success float-left mt-1">
                Submit
              </button>
            </form>
          </div>
        </div>
      </div>
      {temperature && city && (<Temperature temperature={temperature} city={city} />)}
      <Toast show={showErrorToast} onClose={() => setShowErrorToast(!showErrorToast)}>
        <Toast.Header>
          Error
        </Toast.Header>
        <Toast.Body>
          {errorMsg}
        </Toast.Body>
      </Toast>
    </>
  )
}

export default WheatherForm;
