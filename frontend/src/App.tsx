import React from 'react';
import './App.css';
import WheatherForm from "./components/WheatherForm";
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  return (
    <div className="App container">
      <div className="main">
        <WheatherForm />
      </div>

    </div>
  );
}

export default App;
