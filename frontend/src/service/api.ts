import axios from 'axios';
const config = require('../config/config.json');


interface RequestParameters {
  city: string
}

export const request = (params: RequestParameters) => {
  return axios.post(`${config.API_URL}/check-weather`, {
    params
  });
}
